import React from 'react';
import Head from 'next/head';
import Container from '../../components/container';
import Header from '../../components/header';
import ToolsIndexPageContent from '../../components/toolsIndex';
import Layout from '../../components/layout';

export default function Disclaimer() {
  return (
    <>
      <Layout>
        <Head>
          <title>Tools</title>
        </Head>
        <Container>
          <Header />
          <ToolsIndexPageContent />
        </Container>
      </Layout>
    </>
  );
}
