import React, { useState } from 'react';
import Head from 'next/head';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import Container from '../../components/container';
import Header from '../../components/header';
import Layout from '../../components/layout';

export default function CompoundInterestCalculator() {
  const [startingAmountValue, setStartingAmountValue] = useState('');
  const [assumedInterest, setAssumedInterest] = useState('');
  const [yearlyContribution, setYearlyContribution] = useState('');
  const [waitingPeriod, setWaitingPeriod] = useState('');
  const [moreInfo, setMoreInfo] = useState(false);

  function handleFieldUpdate(event, stateSetter) {
    event.preventDefault();
    stateSetter(event.target.value);
  }

  const fields = [
    {
      htmlName: 'startingAmount',
      label: 'Starting Amount',
      unit: '$',
      placeholder: '0.00',
      stateValue: startingAmountValue,
      stateSetter: setStartingAmountValue,
    },
    {
      htmlName: 'assumedInterest',
      label: 'Assumed Interest Rate',
      unit: '%',
      placeholder: '8',
      stateValue: assumedInterest,
      stateSetter: setAssumedInterest,
    },
    {
      htmlName: 'yearlyContribution',
      label: 'Yearly Contribution',
      unit: '$',
      placeholder: '1200',
      stateValue: yearlyContribution,
      stateSetter: setYearlyContribution,
    },
    {
      htmlName: 'waitingPeriod',
      label: 'Waiting Period',
      unit: 'years',
      rightUnitPlacement: true,
      placeholder: '20',
      stateValue: waitingPeriod,
      stateSetter: setWaitingPeriod,
    },
  ];

  function calculateReturn(
    sA,
    aI,
    yC,
    wP,
  ) {
    try {
      let startingAmount;
      let assumedInterest;
      let yearlyContribution;
      let waitingPeriod;
      const timesApplied = 1;

      startingAmount = parseFloat(sA);
      assumedInterest = parseFloat(aI);
      yearlyContribution = parseFloat(yC);
      waitingPeriod = parseFloat(wP);

      console.log(startingAmount);
      console.log(assumedInterest);
      console.log(yearlyContribution);
      console.log(waitingPeriod);

      if (Number.isNaN(startingAmount)) {
        throw new Error('Recieved NaN while parsing.');
      }
      if (Number.isNaN(assumedInterest)) {
        throw new Error('Recieved NaN while parsing.');
      }
      if (Number.isNaN(yearlyContribution)) {
        throw new Error('Recieved NaN while parsing.');
      }
      if (Number.isNaN(waitingPeriod)) {
        throw new Error('Recieved NaN while parsing.');
      }

      let principal = startingAmount;
      let principalByYear = [];
      for (let i = 0; i < waitingPeriod; i += 1) {
        const principalToNumber = Number.parseFloat(principal).toFixed(2);
        principalByYear.push({
          x: i,
          y: principalToNumber,
        });
        principal = (principal + yearlyContribution) * (1 + ((assumedInterest / 100) / timesApplied)) ** (timesApplied);
      }

      const resultInDollarsAndCents = Number.parseFloat(principal).toFixed(2);
      const resultString = `$ ${resultInDollarsAndCents}`;
      const response = {
        finalPrincipal: resultString,
        principalByYear,
      };

      return response;
    } catch (err) {
      console.error(err);
      return {
        finalPrincipal: "Can not calculate.",
        principalByYear: [],
      };
    }
  }

  const calculatorReturn = calculateReturn(
    startingAmountValue,
    assumedInterest,
    yearlyContribution,
    waitingPeriod,
  );

  return (
    <>
      <Layout>
        <Head>
          <title>Compound Interest Calculator</title>
        </Head>
        <Container>
          <Header />
            <section className="flex-col md:flex-row flex items-center md:justify-between mt-16 mb-16 md:mb-12">
              <h1 className="text-6xl md:text-8xl font-bold tracking-tighter leading-tight md:pr-8">
                Compound Interest Calculator.
              </h1>
            </section>
            {
              fields.map((field) => (
                <div key={field.htmlName}>
                  <label htmlFor={field.htmlName} className="block text-sm font-medium text-xl">{field.label}</label>
                  <div className="mt-1 relative rounded-md shadow-sm">
                    <div className={
                      field.rightUnitPlacement ? "absolute inset-y-0 right-0 pl-3 flex items-center pointer-events-none"
                      : "absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none"}>
                      <span className="text-gray-500 sm:text-xl">
                        {field.unit}
                      </span>
                    </div>
                    <input
                      type="text"
                      inputMode="decimal"
                      name={field.htmlName}
                      id={field.htmlName}
                      className="focus:ring-indigo-500 focus:border-indigo-500 block w-full pl-8 pr-12 sm:text-xl border-gray-300 rounded-md"
                      placeholder={field.placeholder}
                      value={field.stateValue}
                      onChange={(e) => handleFieldUpdate(e, field.stateSetter)}
                    />
                  </div>
                </div>
              ))
            }
            <p className="text-xl">
              Result: {calculatorReturn.finalPrincipal}
            </p>

            <br />
            <br />

            <FormControlLabel
              control={
                <Switch
                  checked={moreInfo}
                  onChange={(e) => setMoreInfo(e.target.checked)}
                  name="moreInfo"
                  color="primary"
                />
              }
              label="See more detailed information?"
            />
            {
              moreInfo && calculatorReturn.principalByYear.length ?
                (
                  <>
                    <h2 className="mb-8 text-3xl font-bold tracking-tighter leading-tight">
                      Principal By Year.
                    </h2>
                    {
                      calculatorReturn.principalByYear.map((principal) => (
                        <p className="text-xl">
                          Year {principal.x}: $ {principal.y}
                        </p>
                      ))
                    }
                  </>
                ) : null
            }

            <br />
        </Container>
      </Layout>
    </>
  );
}
