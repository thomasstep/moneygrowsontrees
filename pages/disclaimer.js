import React from 'react';
import Head from 'next/head';
import Container from '../components/container';
import Header from '../components/header';
import DisclaimerPageContent from '../components/disclaimer';
import Layout from '../components/layout';

export default function Disclaimer() {
  return (
    <>
      <Layout>
        <Head>
          <title>Disclaimer</title>
        </Head>
        <Container>
          <Header />
          <DisclaimerPageContent />
        </Container>
      </Layout>
    </>
  );
}
