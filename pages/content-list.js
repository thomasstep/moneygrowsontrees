import React from 'react';
import Head from 'next/head';
import Container from '../components/container';
import Header from '../components/header';
import ContentListContent from '../components/content-list';
import Layout from '../components/layout';

export default function AboutMe() {
  return (
    <>
      <Layout>
        <Head>
          <title>Content List</title>
        </Head>
        <Container>
          <Header />
          <ContentListContent />
        </Container>
      </Layout>
    </>
  );
}
