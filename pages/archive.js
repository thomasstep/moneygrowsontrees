import React from 'react';
import Head from 'next/head';
import Container from '../components/container';
import MonthlyPosts from '../components/monthly-posts';
import Header from '../components/header';
import Layout from '../components/layout';
import { getAllPostsByMonth } from '../lib/api';

export default function Archive({ monthlyPosts }) {
  return (
    <>
      <Layout>
        <Head>
          <title>Archive</title>
        </Head>
        <Container>
          <Header />
          <section className="flex-col md:flex-row flex items-center md:justify-between mt-16 mb-16 md:mb-12">
            <h1 className="text-6xl md:text-8xl font-bold tracking-tighter leading-tight md:pr-8">
              Archive.
            </h1>
          </section>
          {Object.entries(monthlyPosts).map(([month, posts]) => (
            <MonthlyPosts month={month} posts={posts} />
          ))}
        </Container>
      </Layout>
    </>
  );
}

export async function getStaticProps() {
  const monthlyPosts = getAllPostsByMonth([
    'title',
    'date',
    'slug',
    'author',
    'coverImage',
    'excerpt',
  ]);

  return {
    props: { monthlyPosts },
  };
}
