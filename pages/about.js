import React from 'react';
import Head from 'next/head';
import Container from '../components/container';
import Header from '../components/header';
import AboutPageContent from '../components/about';
import Layout from '../components/layout';

export default function AboutMe() {
  return (
    <>
      <Layout>
        <Head>
          <title>About</title>
        </Head>
        <Container>
          <Header />
          <AboutPageContent />
        </Container>
      </Layout>
    </>
  );
}
