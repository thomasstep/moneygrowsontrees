import React, { useEffect } from 'react';
import { useRouter } from 'next/router';
import ErrorPage from 'next/error';
import Head from 'next/head';
import ConvertKitForm from 'convertkit-react'
import Container from '../../components/container';
import PostBody from '../../components/post-body';
import Header from '../../components/header';
import PostHeader from '../../components/post-header';
import Layout from '../../components/layout';
import { getPostBySlug, getAllPosts } from '../../lib/api';
import PostTitle from '../../components/post-title';
import markdownToHtml from '../../lib/markdownToHtml';

const MY_FORM_ID = 1948983;

const config = {
  formId: MY_FORM_ID,
  template: 'mills',
  emailPlaceholder: 'Email address',
  submitText: 'Subscribe',
};

export default function Post({ post, morePosts, preview }) {
  const router = useRouter();
  if (!router.isFallback && !post?.slug) {
    return <ErrorPage statusCode={404} />;
  }

  useEffect(() => {
    const script = document.createElement("script");

    script.src = "https://successful-thinker-9067.ck.page/f11e2417cb/index.js";
    script["data-uid"] = "f11e2417cb";
    script.async = true;

    document.body.appendChild(script);

    return () => {
      document.body.removeChild(script);
    }
  }, []);

  return (
    <Layout preview={preview}>
      <Container>
        <Header />
        {router.isFallback ? (
          <PostTitle>Loading…</PostTitle>
        ) : (
          <>
            <article className="mb-32">
              <Head>
                <title>
                  {post.title}
                </title>
                <meta
                  name="description"
                  content={
                    post.description
                    || post.excerpt
                    || 'A blog about personal finance and investing. Learn how to create a budget, invest wisely, and make more money.'
                  }
                />
                {/* <meta property="og:image" content={post.ogImage.url} /> */}
              </Head>
              <PostHeader
                title={post.title}
                // coverImage={post.coverImage}
                date={post.date}
                author={post.author}
              />
              <PostBody content={post.content} />
            </article>
          </>
        )}
        <script
          async
          data-uid="f11e2417cb"
          src="https://successful-thinker-9067.ck.page/f11e2417cb/index.js"
        />
        {/* <ConvertKitForm formId={MY_FORM_ID} template="clare" /> */}
      </Container>
    </Layout>
  );
}

export async function getStaticProps({ params }) {
  const post = getPostBySlug(params.slug, [
    'title',
    'date',
    'slug',
    'author',
    'content',
    'ogImage',
    'coverImage',
    'description',
    'excerpt',
  ]);

  const content = await markdownToHtml(post.content || '');

  return {
    props: {
      post: {
        ...post,
        content,
      },
    },
  };
}

export async function getStaticPaths() {
  const posts = getAllPosts(['slug']);

  return {
    paths: posts.map((post) => ({
      params: {
        slug: post.slug,
      },
    })),
    fallback: false,
  };
}
