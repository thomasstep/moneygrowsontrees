const fs = require('fs');
const path = require('path');
const { getPostSlugs } = require('./lib/api');

function generateSitemapText() {
  const files = getPostSlugs();
  const postPaths = files.map((slug) => `https://moneygrowsontrees.co/blog/${slug.replace(/\.md$/, '')}`);
  const otherPaths = [
    'https://moneygrowsontrees.co/about',
    'https://moneygrowsontrees.co/archive',
    'https://moneygrowsontrees.co/tools',
    'https://moneygrowsontrees.co/tools/compound-interest-calculator',
  ];
  const allPaths = otherPaths.concat(postPaths);
  const sitemapText = allPaths.reduce((paths, path) => paths.concat('\n', path), 'https://moneygrowsontrees.co/');
  return sitemapText;
}

async function generateSitemap() {
  const siteMapText = generateSitemapText();

  const staticOutputPath = path.join(process.cwd(), 'public');

  fs.writeFile(`${staticOutputPath}/sitemap.txt`, siteMapText, err => {
    if (err) {
      console.log(err);
    } else {
      console.log('File written successfully');
    }
  });
}

generateSitemap();
