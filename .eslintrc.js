module.exports = {
  env: {
    browser: true,
    commonjs: true,
    es2021: true,
  },
  extends: [
    'plugin:react/recommended',
    'airbnb',
  ],
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 12,
  },
  plugins: [
    'react',
  ],
  rules: {
    'import/prefer-default-export': 'off',
    'react/jsx-props-no-spreading': ['error', {
      custom: 'ignore',
    }],
    'jsx-a11y/anchor-is-valid': 'off',
    'linebreak-style': 'off',
    'no-multiple-empty-lines': 'off',
    'no-console': 'off',
  },
};
