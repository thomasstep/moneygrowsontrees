---
title: 'Not Investing Loses You Money'
excerpt: "A solid nest egg in today's terms might be next to nothing a few decades down the line."
descrition: 'Learn how investing keeps you ahead of inflation.'
date: '2020-07-20T05:00:00.001Z'
categories:
  - investing
author:
  name: Thomas Step
---

Money and the purchasing power associated with it are never stagnant. Either you are gaining or losing money, and over a long time, you will lose money if you let it sit around in a bank account. The cause of your money losing its purchasing power is due in part to inflation. Inflation is more or less controlled by the Federal Reserve, and their goal is to average inflation out at [2% over the long term](https://www.federalreserve.gov/faqs/money_12848.htm). Saving cash definitely has its place in a financial strategy, but with the purchasing power decreasing over time you need to try and find a way to keep your money's value up. A solid nest egg in today's terms might be next to nothing a few decades down the line.

Looking at the stock market or average housing prices is a decent way to observe how purchasing power decreases over time. [Check out the average cost of a house with this graph.](https://fred.stlouisfed.org/series/ASPUS) [Here is another chunk of data from the Census Bureau, which echoes the same information.](https://www.census.gov/hhes/www/housing/census/historic/values.html) The amount needed to purchase a home in the 1960s or 70s is drastically different from the amount needed in the 2000s. The stock market follows the same trend of continually increasing in value. Just search for a graph of the S&P 500 for the life of the index. The cost of entry continues to rise; however, the important thing to notice is the rate of change in the stock market and real estate is greater than the inflation goal of the Federal Reserve. This means that investing for the long term will earn you more money than the rate of inflation will take away. By long term, I mean over a decade. I never go into an investment thinking that I will need to pull that money out within a ten year time period. There will be ups and downs, but in the long run, a good investment will make you more money than just sitting in a bank. The S&P 500 has an average annualized return of over 9%, which beats any bank's savings account rate that I have ever seen. I keep my investments simple because beating inflation does not have to be hard.
