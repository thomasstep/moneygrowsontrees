---
title: "My Business Idea Disadvantage"
excerpt: 'Since I am such a hard sell in the first place, I am an even harder sell to myself.'
description: 'Discussing why being so frugal hampers business ideas.'
date: '2020-09-28T06:00:00.001Z'
categories:
  - other
author:
  name: Thomas Step
---

While perusing through an online community called [IndieHackers](https://www.indiehackers.com), I realized a disadvantage that I think I am experiencing due to my view on personal finance. I was in the mood to brainstorm Software as a Service (Saas) and web application ideas, and I was attempting to draw inspiration from previous founders who have posted their successes on IndieHackers. I clicked through some links and visited some websites of successful SaaS businesses mostly to see what they were selling. Some of the ideas and services were unique and interesting, but something was missing for me. I would never exchange money for the services that they were offering. I was not their target market. Then I started thinking about who's target market I would find myself in. The answer I came up with was that I am difficult to market to for one reason: I hate spending money. As fun or interesting as an idea might be to me, getting me to exchange money for it is a challenge.

Where I see my stance on personal finance as a disadvantage is that I find it hard to sell myself on my own ideas. One application that I am building right now is called [Elsewhere](https://elsewhere.now.sh/). I find it useful and ultimately I wanted to build something for myself, not necessarily to sell and turn into a business. A few months into building it, I thought that maybe I could monetize it somehow, and I still have some ideas for potentially monetizing it. At the very least, learning how to turn my product into a business would be a good learning experience. Where I fall short is pricing it. Figuring out pricing has been difficult for me, and it sounds like this is a well-known problem in building a business. Currently, I am toying with the idea of yearly access for $5 or something similar. However, I am having a hard time even selling that idea to myself. That's where the disadvantage comes in. On top of that, other ideas that I have for building a business also seem unrealistic to me because "why would anyone buy it?" The lack of confidence in trying to sell something is a limiting factor. I hope that launching something and figuring out how to make some sort of revenue, no matter how small, will give me a better sense of confidence for building and pricing something in the future.

Since I am such a hard sell in the first place, I am an even harder sell to myself. This can be spun into a positive though because I know that if I am willing to spend money on something, then there will most likely be others out there that are more than willing to spend their money on it. My major expenses month-to-month are housing, food, and transportation. I rarely ever dip into my clothing, entertainment, or "other" budgets. It's still so foreign to me how and why people spend such a large portion of their income on other things, at least before they are financially independent. A normal consumer would have a confidence advantage over me going into a business because they will have been in the average person's shoes and will know what that person is willing to spend on a given product or service.


These were just some thoughts swirling around in my head that I wanted to get out.
