---
title: 'First Post'
excerpt: 'About me.'
date: '2020-06-22T05:00:00.001Z'
author:
  name: Thomas Step
---

Hello my name is Thomas Step, and I love money, personal finance, and investing. My parents started teaching me when I was young, but my curiosity eventually took the reins. I have read numerous books about personal finance and investing, and I constantly read articles to keep myself up to date. I still have tons to learn and put into practice, and I plan on using this blog to talk about what I learn and how I use it. The world of personal finance has a bizarre amount of different rabbit holes to go down, and some of the rabbit holes that I have poked around in are FIRE (Financial Independence Retire Early), financial minimalism, budgeting, real estate investing, and investing in the stock market.

I was born in 1995, and I am lucky enough to be a software engineer, which means that I am making a pretty good amount of money right off the bat. If everything goes to plan, I will not have to worry about having a large enough nest egg for retirement. That being said, my plan is drawn out over decades, and a lot can happen until then. Until I get to the end goal, I am looking for ways to not take on debt, boost my income, and invest strategically. My blog will most likely be aimed at an audience that is around my age; however, the principles and foundation that I have learned about and will talk about can be applied to just about anyone.

It seems like there are numerous voices all over the internet about how to get out of debt. Debt is such a large topic because so many people have it but do not want it. While I might not always specifically write about that topic, I believe that it all boils down to one principle: spend less money than you make. This was ingrained in me for most of my childhood until I became independent, and I think it is something that we should all live by. Of course that is easier said than done. My goal is to continue spending less than I make, continue saving money, continue investing, and hopefully at the end of it all come out without having to worry about a paycheck.


