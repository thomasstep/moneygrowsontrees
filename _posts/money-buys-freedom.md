---
title: 'Money Buys Freedom'
excerpt: "You can create a lifetime's worth of money is less than a lifetime."
description: 'Describing how and why money can buy freedom'
date: '2021-03-21T06:00:00.001Z'
categories:
  - earning
author:
  name: Thomas Step
---

Way back when everyone provided for themselves, they couldn't store up years worth of food or it would go bad. Today, we can create a lifetime's worth of money in less than a lifetime. Just the idea of money is powerful in the sense that we can store wealth with it.

When I think of older times, I think of most people farming to grow plants and raise animals to provide enough food for themselves and their families only to repeat the process year in and year out. Food can store for a little while, but not forever. Besides storage, there are specific growing seasons and space constraints on how much food a given person could produce in a year. People had to continue working because there were not many other options. Growing enough food for two years in one year would end with spoiled food before the worker could eat it. However, with money, we can capture the amount of work or value someone has brought into the world indefinitely. If a person works more than they need to in a given timeframe, they get more money in return for that work and by those means can store their contributed value.

Storing value is a great way that money helps us move towards freedom. A person can work or contribute more value than they consume and later on decide to consume that value. This is where a budget would fit in to determine how much more someone is contributing than consuming. Someone who consumes as much as they contribute will probably not have the opportunity to retire when they want since they have not stored up enough reserves. However, someone who consumes far less than they contribute will have the opportunity to stop working in the future. The less someone consumes, the faster they can stop working to live off of their stored work. Some have taken this so far as to only work a couple of decades or less before quitting work to do whatever they chose to. They have figured out and utilized the idea that money can buy freedom.

Work translates into money. Money translates into providing for ourselves. Saving enough money to provide for oneself indefinitely translates to freedom. Once a person has contributed so much to society that they can allocate their resources in a way to provide for themselves without working anymore, they have bought themselves freedom. That freedom may or may not include working. I hope to get to that point someday soon, but I believe that I will still want to work on something. Lounging around indefinitely might seem nice right now, but I know that I will eventually get bored of that and want to work on something. Whether or not what I work at will be high paying or not will be the difference between what I spend my days doing now and what I will spend my days doing in the future. However, I do enjoy writing software now, and I want to keep pursuing that passion until it no longer pleases me.

The freedom that money can buy shows the difference between what a person contributes and what a person consumes. Contributing more than consuming means having the opportunity to store up work and created value. Storing up enough of one's contributions could lead to freedom that only money can buy.
