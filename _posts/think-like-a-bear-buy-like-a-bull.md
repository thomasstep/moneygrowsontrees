---
title: 'Think Like A Bear, Buy Like A Bull'
excerpt: 'Arguments have two sides and there is always an argument going on about the stock market.'
description: 'Mindset to stick to an investing strategy'
date: '2021-03-29T06:00:00.001Z'
categories:
  - investing
author:
  name: Thomas Step
---

Arguments have two sides and there is always an argument going on about the stock market. Stock prices are not stagnant and people constantly bet (or at least contemplate) on whether or not prices will rise or fall. Anyone who has ever looked at financial news has most likely read about the bulls and the bears. Bulls bet that prices will rise, while bears bet that prices will fall. Those bets can be over short or long periods but the sentiment remains. As in many facets of life, some choose to agree and act wholeheartedly with one side. I choose to take pieces of each to construct a better plan of how to spend my money in the markets.

Bears think everything is overpriced and will go down. When prices go up, bears have even more justification to say prices have to drop. The reasoning behind bears' sentiment is varied with some investors out there (think Ray Dalio or Michael Burry) providing sound arguments as to why the markets as a whole are priced so high right now and why they can not stay this bloated forever. That being said, people who believe that everything is going to have to crash down do not have much incentive to buy into the markets. Why buy something that you wholeheartedly believe is overvalued?

Bulls think everything is set to rise. When it goes down, those same bulls might panic because they weren't prepared for it. Once markets start reaching new record highs as they have been lately, bulls do not always use sound reasoning, and they have to justify their positions purely on guesses and estimates based on data and trends. However, if everyone acted like a bull, there would be a self-fulfilling prophecy. If everyone thought prices would rise, odds are strong that those people would buy into the market. As people continue to buy in, prices would continue to go up, thus the bulls would prove themselves right.

I have looked at both the bears' and the bulls' arguments from the perspective of a time with markets at their peaks like today (March 2021), but what about the bear versus bull arguments after a crash or correction? I am no psychologist, so my views on how investors think are sure to be flawed and limited. Take this with a grain of salt.

During a price drop one of two things will happen to any investor in either mindset, they will hold to their position or they will change sides. From a bear's perspective, if the price drops they will either think "now the price is fair" or "this has only just begun." In the first case, they are likely to switch sides, become a bull, and begin to buy in. In the latter, they will continue to bet on prices shrinking. The opposite would then hold for a bull, and they would either think "this price is a great deal" or "I was wrong, I better get out now". The first case having them stick to their guns, and the latter having them switch sides. Let's look at those thought processes in a little more detail.

On both sides, one of the thought examples was ultimately the same, "the price dropped and now it is a good deal." This is the right mindset about prices, at least for someone with a long investment horizon like myself. In forty years, most people would argue that stock prices will go up, so any time that prices drop significantly is a great sale for me.

One thought example that sticks out to me as being wildly inefficient is the bear thinking "this [price drop] has only just begun." I have one question for a person of that mindset: when will you ever buy? That investor is not much of an investor if they never buy anything because they think everything is overvalued. What if prices never drop to rock bottom? When will that person invest?

The final thought example is scary and potentially more common, which is the bull thinking "I was wrong, I better get out now." In this case, the investor would just be following everyone else, and their mentality would be easily flipped. Instead of having their own thoughts, they just follow the general sentiment. Buying when the momentum is already going up and selling while it is already on its way down is not a great strategy.

The way I see it, there is a happy medium and we have already seen it in the previous examples. I have a long time horizon for my investments which helps me come to my mindset of thinking like a bear and buying like a bull. Thinking like a bear means being prepared for prices to drop while buying like a bull means seeing any drop as a sale and buying at every dip. However, this is easy to say and hard to do.

One way that I have been able to force myself into this habit is by setting up automatic investments at regular intervals whenever I make money. I took the time to set this up once, and now, I don't have to fight myself if the market is down and it comes time to buy. The hardest part of investing to me is knowing that I could potentially lose money. I reassure myself that my long runway will most likely pan out to be beneficial to my future self. Setting up automatic investments helps combat that mental aspect of buying something that could lose me money.

At the end of the day, what I am trying to do with this saying is prepare myself to see negative returns but reassure myself that buying in at the drops is a great deal. By splitting the emotional portion of investing away from the reasoning and strategy, helps me stick to my investing plan.
