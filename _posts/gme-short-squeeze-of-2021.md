---
title: 'GME Short Squeeze Of 2021'
excerpt: "I truly believe this week's actions are going to go down in history for better or for worse."
description: 'Discussing and reporting on the GME short squeeze of 2021'
date: '2021-01-27T06:00:00.001Z'
categories:
  - other
  - investing
author:
  name: Thomas Step
---

EDIT: This is an actively worked on post that will be updated as events unfold and we learn new information.

It is currently Wednesday, January 27, 2021, and I had to start writing about GameStop now. I truly believe this week's actions are going to go down in history for better or for worse. As of today, GameStop has gone up 1,500% in value over the last two weeks due to a short squeeze, and who knows how much more it will go up.

First, some back story. In June of 2019, there was a Reddit user who goes by the name u/DeepFuckingValue (DFV) that posted on [r/wallstreetbets about owning GameStop call options](https://www.reddit.com/r/wallstreetbets/comments/d1g7x0/hey_burry_thanks_a_lot_for_jacking_up_my_cost/). Deepfuckingvalue's first post showed his purchase history for $8 GameStop call options that expired January 15, 2021.

![r/DeepFuckingValue's first post of his GME position in June 2019](/assets/blog/gme/dfvs-first-post.png)

Amidst a falling share price, he claimed that the company's fundamentals were decent and that he thought it was a buy. Despite e-commerce taking over every facet of our lives, DFV believed in it. He dumped around $53,000 into call options. Soon after DFV's purchase, Michael Burry (the big short) came out saying that GameStop was fundamentally undervalued for one reason: game discs. The new generations of PlayStation and Xbox were still offering disc drives for users who did not want to download games from the internet.

Fast forward to 2020. We have been through a pandemic and economic uncertainty. Stores have been closed down, and GameStop in particular has had run-ins with the government because they did not want to close their stores. DFV held his positions. In August of 2020, GameStop announced that they saw an uptick in sales of over 500% due to store closures. The billionaire founder of Chewy, Ryan Cohen, also took a 9% stake in the company saying that he was going to help turn it around and make it into an Amazon competitor. This made total sense coming from someone who made their money in e-commerce. Just two months later in October, GameStop announced that they were entering a strategic partnership with Microsoft, the makers of the Xbox. Cohen increased his stake in GameStop. DFV continued to hold his positions.

Along came another Reddit user by the name of u/airdoon. [Airdoon discovered that GME was one of the most shorted stocks at the time (around September 2020)](https://www.reddit.com/r/wallstreetbets/comments/ikpgw9/gme_short_squeeze/) with 55 million of the 65 million shares being short. (It has come to my attention that airdoon's post has been removed from the Reddit link above. Luckily, [I have a text version of the original posthere](/airdoon-reddit-post.txt). Enjoy.) He explained how under the right circumstances a short squeeze could trigger sending the stock price to the moon. There is some explaining to do around short selling and short squeezing. See, a short sell pretty much means that an investor asked for a certain amount of shares from their broker and guaranteed to give back the same amount of shares at a given time. In the meantime, the investor can sell the shares to someone else on the market. Now, the only way that this makes any financial sense is if the stock price falls. The investor can then buy shares back from the market at a lower price and return the shares to their broker. However, if the opposite happens (share price increases) then the investor will have to buy shares from the market at a higher price than they paid and lose money. A short squeeze happens when there are not enough non-shorted stocks available in the market for the investors who bought the shorted stocks to buy and cover their positions with their brokers. Effectively, the short investors will have to keep offering more and more for shares until one of the shareowners decides to sell it to them. The problem with all of this for the short investors is that there is a theoretically unlimited downside. I'll let that sink in. Unlimited downside. Why someone would want to take on the potentially unlimited downside is beyond me, but here we are.

Reddit now knew how to squeeze these short-sellers into paying copious amounts of money to close out their positions with all of this starting to come into play last week. Redditors of r/wallstreetbets started figuring out that if they bought call options, they could force their brokerages to go out and buy those underlying shares and force the price up. Simple supply and demand when you stop to think about it all. If more people are buying a stock than selling it, the price starts to increase with demand. Once the short sellers start to realize that the share price is increasing, they might also start to buy shares to close out parts of their positions further increasing demand and therefore share price. All of this sort of compounds into the price going to the moon. However, there is a catch to this mastermind of a plan. No one can sell. That's right. If shareholders start to sell their positions, that means more shares will be available on the market and the short-sellers can close out their positions; albeit at quite a loss. The current plan of r/wallstreetbets is to hold their positions, which forces the short sellers to offer more and more money to try and get someone to sell. This is where we currently are in the tug-of-war between the retail investors of Reddit and the "smart" professional institutional investors of Wall Street.

I will come back and update this post with any new details until the bubble deflates. No one knows how this is going to end up, but it will probably turn out to be quite polarizing. The share price could skyrocket or it could fall on its face depending on the shares available to short-sellers trying to cover their positions. I'm super excited to see how this unfolds.

Before I close out my writing for Wednesday, remember DFV who bought those calls back in 2019? His initial $53,000 bet is now valued at $47,973,298. Here's his screenshot for proof.

![r/DeepFuckingValue's GME position on Wednesday, January 27, 2021](/assets/blog/gme/dfv-wednesday.png)

Update for Thursday, January 28:

Trading has opened and GME is currently up. Robinhood has blocked its users from buying more shares of GME but will allow them to sell. I imagine that will come back to bite them in the butt because it looks like market manipulation. There are wild waves of buying and selling with speculation that it is all between the hedge funds themselves to try and simulate a lower price. The goal of that would be to scare investors into letting go of their shares so the hedge funds can buy at a lower price and cover their short positions. No one knows what is truly happening behind the scenes, but if the Redditors hold their positions the hedge funds' buying and selling would effectively cancel their price manipulations out.

Today gained even more media coverage about what is happening, why it is happening, and what might happen moving forward. After Robinhood barred trading, a few other brokerages followed and halted trading extended to other speculative stocks with large short positions like Blackberry and AMC. The brokerages with closed down trading came under heat from high profile individuals, investors, and politicians like [Mark Cuban](https://twitter.com/mcuban/status/1354798465994338304), [AOC](https://twitter.com/AOC/status/1354830697459032066), [Ted Cruz](https://twitter.com/tedcruz/status/1354833603943931905), and [Ben Shapiro](https://twitter.com/benshapiro/status/1354795139500302338).

Trading has ended for the day with GameStop down to $193 per share, which is 44% lower than opening. After-hours trading, as of 5:00 PM CST, is pricing GME at around $290, which is a 50% increase from close. Plenty of investors holding GameStop must have experienced an incredible decrease in wealth on paper, but there is no way it was near as bad as the largest short, Melvin Capital, who supposedly will be taking losses into the billions. [Our old friend DFV lost a nauseating $14.8 million](https://www.reddit.com/r/wallstreetbets/comments/l78uct/gme_yolo_update_jan_28_2021/) on paper.

![r/DeepFuckingValue's GME position on Thursday, January 28, 2021](/assets/blog/gme/dfv-thursday.png)

At the end of the day, the price increase is due to the simple economics of supply and demand. The Redditors have realized that their shares are worth more than what they paid for them by a select few people out there. The supply has dried up and short-sellers are looking to buy shares to cover their positions and mitigate losses to interest on margin. For that sole reason, the price of GameStop could very well continue to increase tomorrow as short-sellers look to close out their positions. The price could also remain the same, who knows.

I will also point out that this is a rather interesting social experiment. The investors holding onto shares are more or less [stuck in a prisoner's dilemma](https://en.wikipedia.org/wiki/Prisoner%27s_dilemma). Betrayal in this scenario would be selling shares, which would lower prices and allow short sellers to close out their positions, and cooperation would be holding shares, which would increase prices as demand swells to mitigate losses by short-sellers. Looking back on this event, we will be able to tell the price which started coaxing investors into selling shares. What it will come down to is how much the short sellers will offer before the masses start to sell-off. That price could be $500 or it could be $1,000. Only time will tell, but either way, this bubble will pop.

Update for Friday, January 29:

GME was up around 70% at closing compared to the open. There were loads of Reddit posts and media articles of people trying to wrap their heads around what's happening. The most interesting part for me is the amount of support for the Redditors. There seems to be a lack of trustworthy data out there, but it does seem like the float is still over 100% meaning that a squeeze can still occur. Judging by the price of shares not plummeting today, I would argue that Reddit is sticking to its plan of holding, and it seems to be working for now.

Another interesting development is the [Wall Street Journal article that came out about DFV himself](https://www.wsj.com/articles/keith-gill-drove-the-gamestop-reddit-mania-he-talked-to-the-journal-11611931696). I would not want to show my face if I were in his shoes, but here we are.

Robinhood is still up to their shenanigans by limiting users to only 5, then 2, then 1 share of GME ownership and also exercising their options without warning. As I am not a Robinhood user myself, I can not verify that these claims are true; however, it seems like a bad avenue for Robinhood to go down. There are already lawsuits being filed against them and a huge wave of Redditors moving brokerages.

We have yet to see the "squeeze of all squeezes" on the short-sellers, but people still expect it to come. Right now, the outcome is largely based on which side will break first, the current GameStop shareholders selling their shares or the short-sellers submitting expensive buy orders and losing money. There have already been billions lost by the hedge funds, but they have not completely covered their positions. I guess that they just want to draw this game of chicken out and try to make the decentralized side cave first.

Lastly, DFV updated Reddit on Friday after market close with a post [showing his almost $12.9 million gain in value for his GameStop position](https://www.reddit.com/r/wallstreetbets/comments/l846a1/gme_yolo_monthend_update_jan_2021/).

![r/DeepFuckingValue's GME position on Friday, January 29, 2021](/assets/blog/gme/dfv-friday.png)

<!-- Update for whenever:

Apparently this guy almost single handedly kept GME's price above $320 on Friday by dumping $640 million in.
https://www.reddit.com/r/wallstreetbets/comments/l9qtey/kjetill_stjerne_is_da_real_mvp_he_his_friends_are/

Some DD about covering short positions
https://www.reddit.com/r/wallstreetbets/comments/lalucf/i_suspect_the_hedgies_are_illegally_covering/ -->
