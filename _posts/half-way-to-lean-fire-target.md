---
title: 'Half Way to Lean FIRE Target'
excerpt: 'My net worth is now half of what I expect that I need to hit Lean FIRE.'
description: 'Discussing Lean FIRE and what it means as a milestone.'
date: '2021-05-06T06:00:00.001Z'
categories:
  - growing
author:
  name: Thomas Step
---

In my post reflecting on 2020 and [what that meant for my finances](https://money.thomasstep.com/blog/financial-look-back-on-2020), I mentioned that I was about 15% of the way towards the amount of money I wanted invested to feel I had reached financial independence. Due to the continued market run up, I am now even closer, but I wanted to also celebrate another milestone. My net worth is now half of what I expect that I need to hit Lean FIRE.

While Lean FIRE is not at all what I am going for, I think it is still a worthwhile benchmark to gauge how I am doing. My Lean FIRE number is a sort of checkpoint before hitting my larger number and something that not many people in the world can say they have achieved. I realize that I am extremely fortunate to be in those ranks, but I also realize that I put in the necessary work and followed a process to get here.

Hitting Lean FIRE means that someone can draw off of their investment portfolio at around 4% annually and be able to provide the most basic necessities for themself. It seems as if most Lean FIRE folks run their numbers based off of what they currently spend, which I see as a total downside. To me, Lean FIRE seems somewhat like an oxymoron and I can't imagine that many people in the FIRE community actually retire in a Lean manner. The community (and myself especially) are all about planning for the long road and financial security. Basing early retirement off of a number that someone currently spends seems like the opposite of planning for the long road. Other expenses could pop up like medical, family-related, or (a topical conversation) inflation. Not only could there be unforeseen expenses, but our theoretical Lean FIRE participant could decided that they want to take more vacations now, live in a nicer house, or generally let their lifestyle inflate. Being truly Lean FIRE'd would not allow enough room for extra expenditures. To take from a show I enjoy, "for those reasons, I'm out."

All that being said about Lean FIRE though, I do still think it's an important milestone because it means that someone is on track to becoming financially independent. Knowing that one's investments could potentially carry them over a rough patch like job loss, or a medical emergency must be reassuring. While I don't agree with living Lean FIRE out, I do believe that it is a viable solution for getting around a short term problem without one's finances being hit particularly hard. For that reason, I want to celebrate that number when it happens, and I want to celebrate the half-way marker that I have achieved.

Stocks are weird and the notion that my money has decided to grow on its own is also weird. I have [discussed the reasons why before](https://money.thomasstep.com/blog/money-is-just-a-representation-of-value) so I won't go into them now. It's fun to be able to look back on what I have done over a few short years and know that the process that I have stuck to has started to pay off. While it might not seem like I have made much on my investments in terms of actual value, I know that this is just the beginning and it will take off out of my control sooner or later.
