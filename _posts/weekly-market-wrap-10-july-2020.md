---
title: 'Weekly Market Wrap: 10 July 2020'
excerpt: 'For the week of July 6 to July 10'
description: 'Market update for the week of July 6 to July 10'
date: '2020-07-10T05:00:00.001Z'
categories:
  - investing
author:
  name: Thomas Step
---

My strategy is still remaining the same. I plan on keeping my financial automation in place and consistently buying into the market. That being said, the cash that I have waiting in reserves, is looking more and more valuable. I am no economic genius and I am no licensed or certified accountant, but the markets are getting closer and closer to where they were before COVID ever really hit the US except now we are living in a significantly different economic climate. Even though [employment reports improved last week](https://www.cnbc.com/2020/07/02/jobs-report-june-2020.html), I think that it is incredibly possible that we could see another sell-off soon during earnings season coming up. There is also an [uptick in daily cases](https://www.cnbc.com/2020/07/10/us-reports-record-single-day-spike-of-63200-new-coronavirus-cases.html), which I do not think is helping anything out. The combination of potentially bad earnings reports and increased coronavirus cases seems like a recipe for negative news headlines.

It seems to me like the Fed has done what they set out to do in making sure that the markets did not completely tank, but what they do not have the power to do is control the emotions of all the investors out there. In my opinion, the stock market ups and downs are due to news. If major news outlets decide to start flashing headlines about extremely poor earnings and bad outlooks, then people are going to sell. I don't think there is a way around it because there is no way to change the emotions that someone is feeling. If you stay level headed though, you can scoop up some nice deals. Overall, I am still extremely long on my investment position. I am not attempting to time anything, I am not going to sell everything, I am simply going to buy more than normal if a good deal comes my way.
