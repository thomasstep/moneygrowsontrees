---
title: 'How Investing and Compound Interest Build Wealth Over Time'
excerpt: 'A little saving up front does wonders later on because compounding interest is a powerful thing.'
descrition: 'Learn about compounding interest and how it drastically increases wealth.'
date: '2020-07-13T05:00:00.001Z'
categories:
  - basics
  - investing
author:
  name: Thomas Step
---

I heard about a concept once that talked about a person's lifetime earnings potential instead of a yearly salary or hourly wage. I had never thought about earning potential like that before, but it is a good way to put perspective on how much money you are actually spending and force you to think about the future. As of this writing, [the mean personal income sits somewhere around $50,000 in the US](https://fred.stlouisfed.org/series/MAPAINUSA646N). If we assume that a person starts working at 18 years old until the retirement age of 67, that person would make $2,450,000 in their lifetime. For the sake of this example, I will not include inflation or wage changes because that would complicate the point that I am trying to make. Hitting in the millions of dollars does not seem too bad; however, this is just income and there is a certain cost that we need to associate with living. I will assume that this theoretical person manages to live on half of their income until they hit retirement; they now have $1,225,000. In this person's life, they have managed to spend $1,225,000 on just being alive for 49 years. When you look at $25,000 per year on expenses, it does not seem that bad, but over 49 years, that money really adds up. I think calculations like this are fun and make a good point, so I'm going to continue through a few scenarios.

Let's say our person loves to buy clothes and budgets $50 per month on clothes shopping. That means they spend $600 per year or $29,400 at the end of that 49 year period. The amount of money they now have is $1,195,600. Our theoretical spender also likes a night out to eat followed by a trip to the bar with their friends. They only do it twice per month, but it costs them $100 each time after all is said and done. Just to be fair, we will only calculate this from the age of 21-40. Our spender plows through $45,600 on going out to eat and drinking in a 19-year span; they now have $1,150,000. In the same vein, our spender likes to kick back and relax on a beach once or twice a year for vacation. If they spend about $2,000 per year on vacations they total $98,000 over their 49 year working time. They now have $1,052,000. That $1 million mark still seems pretty nice, but think about all of the other ways that you could spend your money. Every extra dollar you spend takes one dollar away from this example. After the clothes shopping, going out, and vacations that this theoretical person takes, they have spent an extra $5,000 per year or 10% of their annual income. This means that if the average salaried person saves 40% of their income, they can have a little over $1 million in cash by the end of their working life. That $1 million could definitely last them until the end of their expected life at 72. This theoretical person just traded their time for money. Almost all of their time.

Let's say our person decides to not spend any more than 50% of their income and the other 50% goes straight into an investment account. The S&P 500 has averaged a 9.8% annualized return for the last 90 years. If our person puts that 50% (or $25,000 per year) into a mutual fund that tracks the S&P 500 index for 49 years, [they will have over $27 million after 49 years](http://www.moneychimp.com/calculator/compound_interest_calculator.htm).

![Average S&P 500 Return on $25,000](/assets/blog/how-investing-and-compound-interest-build-wealth-over-time/25kPerYearInvested.png)

Using the person who spent a little extra every month for the same investment example, if they put 40% (or $20,000 per year) into the same S&P 500 mutual fund, [they would over $21 million after 49 years](http://www.moneychimp.com/calculator/compound_interest_calculator.htm).

![Average S&P 500 Return on $20,000](/assets/blog/how-investing-and-compound-interest-build-wealth-over-time/20kPerYearInvested.png)

Investing your money matters. Save and invest as much as you can early on so that you can be in the same spot. Once you cross a certain threshold, you will become financially independent. Once you become financially independent, you get to choose whether or not you want to work anymore. Once you get to choose whether or not you work, you get total freedom over how you want to run your life. A little saving upfront does wonders later on because compounding interest is a powerful thing. The point of all this is to show that if you forgo the little things early on in life, if you keep your expenses to a minimum, and if you work diligently to invest as much as you can, you can build an enormous amount of wealth without having to put in any extra time.
