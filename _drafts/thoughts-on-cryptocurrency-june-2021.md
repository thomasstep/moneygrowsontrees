i'm a software engineer so i'm a proponent of technological advancement
at the beginning of the end and end of last year i was a big proponent of cryptocurrency
what changed my perspective is probably the same as everyone else; energy consumption

i think crypto could be an awesome technology
the idea that there are no middlemen and that the entire world can share the same currency sounds like a logical step forward
between industry "disruptors" and globalization, this seems like it fits in

devil's advocate:
the difference is the measurable energy consumption and bad rap sheet
the same progressive people in the world (at least in the US) that are likely to adopt crypto also want to reduce carbon emissions
this produces an oxymoron of sorts since we can measure that impact crypto mining has on energy consumption
on top of that, there is nothing backing crypto
fiat currencies get their value because an entire nation says there is value
cryptocurrencies get their value because random people on the internet say there is value
doesn't have the same effect
that being said, if industries adopt it wholeheartedly, then there could be a shift since such large forces of value claim that there is value
at some point cryptos would have to stop pegging their value to other currencies though and need to peg their value to actual goods and services
without businesses accepting crypto directly, it's only ever going to be a veil over an actual currency in which case we're just doing what we always did with the added expense of crypto mining