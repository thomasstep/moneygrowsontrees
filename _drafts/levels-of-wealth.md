---
title: 'tbd'
excerpt: 'tbd'
description: 'tbd'
date: '2020-11-16T06:00:00.001Z'
categories:
  - basics
  - other
  - earning
  - investing
  - growing
author:
  name: Thomas Step
---

basic: can cover food, water, protection from the elements. not thriving but surviving

modern day conveniences: car, internet, computer, phone, extra clothes, etc.

luxury: travel, inflated lifestyle, multiple houses

ultra-wealthy: billionaires, stupid money, so much that there's no point

https://www.reddit.com/r/AskReddit/comments/2s9u0s/what_do_insanely_wealthy_people_buy_that_ordinary/cnnmca8/?utm_source=share&utm_medium=web2x&context=3