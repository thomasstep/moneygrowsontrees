---
title: 'Cost of Living'
excerpt: 'tbd'
date: '2020-07-27T05:00:00.001Z'
author:
  name: Thomas Step
---

It costs money to live.
Try to reduce the cost associated with how you live.
Lower your spending to increase your savings and investments.

Unfortunately, being alive costs a lot of money for most people. Being able to travel to your job costs in transportation, being online costs in internet and electricity bills, being a human being costs in grocery bills, and being inside costs in rent, a mortgage, and/or property taxes. There is no real way around it. Living in an area that provides even baseline services costs in taxes. Nothing is free. Necessary expenses like these are your cost of living. As a thought experiment, think about any given geographic location and there will be a minimum amount of money associated with it for a basic "normal" type of life. Food, transportation, and housing costs will probably take up a bulk of the cost of living in just about any location. The more densly populated or popular a place is, the higher that associated cost normally is. Just think of how much gas in Los Angeles costs compared to Houston or how much housing in New York City would cost compared to a rural city in Nebraska. This type of cost is also apparent through cost of living calculators like the one on [Nerd Wallet](https://www.nerdwallet.com/cost-of-living-calculator). Researching where you are going to live and the average cost of living should be considered when you are thinking about moving for any reason including a job.

While I do not think that there is a huge reason to decide on where to live based solely off of cost of living, I think it should factor into that decision. For example, if you are coming from a small town are choosing to take a job in the Bay area, you need to figure out just how much of a salary bump you would need to compensate for a higher cost of living. Drastic differences might make your finances a little more chaotic. That being said
