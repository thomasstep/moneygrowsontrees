---
title: 'Wealth Is Measured In Time Not Money'
excerpt: 'tbd'
description: 'tbd'
date: '2021-02-01T06:00:00.001Z'
categories:
  - basics
  - other
  - earning
  - investing
  - growing
author:
  name: Thomas Step
---