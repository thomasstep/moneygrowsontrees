import React, { useEffect } from 'react';
import Container from './container';
import { EXAMPLE_PATH } from '../lib/constants';

export default function Footer() {
  return (
    <footer className="bg-accent-1 border-t border-accent-2">
      <Container>
        <div className="py-28 flex flex-col lg:flex-row items-center">
          <div className="flex flex-col lg:flex-row justify-center items-center lg:pl-4 lg:w-1/2">
            <a
              href="/"
              className="mx-3 font-bold hover:underline"
            >
              Home
            </a>
            <a
              href="/about"
              className="mx-3 font-bold hover:underline"
            >
              About
            </a>
            <a
              href="/archive"
              className="mx-3 font-bold hover:underline"
            >
              Archive
            </a>
            <a
              href="/tools"
              className="mx-3 font-bold hover:underline"
            >
              Tools
            </a>
            <a
              href="/content-list"
              className="mx-3 font-bold hover:underline"
            >
              Content List
            </a>
            <a
              href="/disclaimer"
              className="mx-3 font-bold hover:underline"
            >
              Disclaimer
            </a>
          </div>
        </div>
      </Container>
    </footer>
  );
}
