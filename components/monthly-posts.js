import React from 'react';
import PostPreview from './post-preview';

export default function MonthlyPosts({ month, posts }) {
  return (
    <section>
      <h2 className="mb-8 text-5xl font-bold tracking-tighter leading-tight">
        {month}
      </h2>
      <div className="grid grid-cols-4 row-gap-12">
        {posts.map((post) => (
          <div className="col-span-4 md:col-start-2 md:col-span-2">
            <PostPreview
              key={post.slug}
              title={post.title}
              coverImage={post.coverImage}
              date={post.date}
              author={post.author}
              slug={post.slug}
              excerpt={post.excerpt}
            />
          </div>
        ))}
      </div>
    </section>
  );
}
