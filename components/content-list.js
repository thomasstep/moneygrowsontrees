import React from 'react';

export default function ContentListContent() {
  return (
    <>
      <section className="flex-col md:flex-row flex items-center md:justify-between mt-16 mb-16 md:mb-12">
        <h1 className="text-6xl md:text-8xl font-bold tracking-tighter leading-tight md:pr-8">
          Content List.
        </h1>
      </section>
      <div className="grid grid-cols-4 row-gap-12">
        <div className="col-span-4 md:col-start-2 md:col-span-2">
          <h2 className="mb-8 text-5xl font-bold tracking-tighter leading-tight hover:underline">
            <a href="https://www.youtube.com/watch?v=wLn28DrSF68" target="_blank" rel="noopener noreferrer">
            Howard H. Stevenson: Building a Life
            </a>
          </h2>
          <div>This is a great video. Full of attention-worthy advice. The following quotes were some of my favorite from Howard's talk.
          </div>
          <ul class="list-disc">
            <li>"Success is a tough problem. The external measures and the internal measures aren't always the same. Sometimes we're rewarded for things we're not proud of, and sometimes we're proud of things we're not rewarded for."</li>
            <li>"Success is getting what you want, happiness is wanting what you get."</li>
            <li>"I hate the word balance...unfortunately, it's about juggling. You have to keep your eye on all the balls, when you touch something you have to give it energy, you have to give it direction."</li>
            <li>"You need someone to talk to whether that's a spouse or a friend. You need to know how to make yourself happy. You need something to achieve."</li>
            <li>"You can't achieve in all dimensions."</li>
            <li>"Things change."</li>
            <li>"That was a choice I chose, I chose it conscously, and I have no regrets, but I still wince."</li>
            <li>"Everybody's outsides look better than my insides."</li>
            <li>"Everything about the future is a bet."</li>
          </ul>
        </div>
        <div className="col-span-4 md:col-start-2 md:col-span-2">
          <h2 className="mb-8 text-5xl font-bold tracking-tighter leading-tight hover:underline">
            <a href="http://www.paulgraham.com/wealth.html" target="_blank" rel="noopener noreferrer">
              Paul Graham: Wealth
            </a>
          </h2>
          <div>This is an essay written by Paul Graham, founder of YCombinator. He explores the meaning of wealth and how it is created. There is a twist towards focusing on software and technology because that is where he made his money. The portions that are more general in nature are the earlier sections of his essay. This essay is similar to Naval Ravikant's message just a few entries below, and I am curious whether or not Naval found inspiration in this exact essay when he created his content.</div>
        </div>
        <div className="col-span-4 md:col-start-2 md:col-span-2">
          <h2 className="mb-8 text-5xl font-bold tracking-tighter leading-tight hover:underline">
            <a href="https://www.youtube.com/watch?v=8-Li_sFNc4Q" target="_blank" rel="noopener noreferrer">
              Mr. Money Mustache: Early Retirement in One Lesson
            </a>
          </h2>
          <div>Mr. Money Mustache gained influence by being a blogger about financial indepence. In this talk, he discusses why everyone can be financially independent and how to do just that. One of the reasons I like him and this video are because his message is similar to mine about simply not buying unnecessary stuff and investing what is left over.</div>
        </div>
        <div className="col-span-4 md:col-start-2 md:col-span-2">
          <h2 className="mb-8 text-5xl font-bold tracking-tighter leading-tight hover:underline">
            <a href="https://nav.al/rich" target="_blank" rel="noopener noreferrer">
              Naval Ravikant: Rich
            </a>
          </h2>
          <div>Naval is a Silicon Valley angel investor. In this series of tweets, podcasts, and posts, Naval explores what wealth actual means and how it is created. He talks less about finance and focusing on delivering value to people.</div>
        </div>
        <div className="col-span-4 md:col-start-2 md:col-span-2">
          <h2 className="mb-8 text-5xl font-bold tracking-tighter leading-tight hover:underline">
            <a href="https://www.amazon.com/gp/product/1612680178/ref=as_li_tl?ie=UTF8&tag=moneygrowso07-20&camp=1789&creative=9325&linkCode=as2&creativeASIN=1612680178&linkId=ac9fdd727168c8a55ebbeaa40f002a49" target="_blank" rel="noopener noreferrer">
              Robert Kiyosaki: Rich Dad Poor Dad
            </a>
          </h2>
          <div>Robert Kiyosaki has been a somewhat controversial figure lately. I have heard that "Rich Dad" did not actually exist, and that the book only got popular because of a MLM scheme. Either way, what I got out of the book was the foundation of why we should save and invest, and ultimately this was the driving force that got me to look deeper into personal finance and investing.</div>
        </div>
      </div>
    </>
  );
}
