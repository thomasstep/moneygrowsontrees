import React from 'react';
import Link from 'next/link';

export default function ToolsIndexPageContent() {
  const tools = [
    {
      name: 'Compound Interest Calculator.',
      link: 'compound-interest-calculator',
      description: 'Calculate how compound interest can grow your money.',
    },
  ];

  return (
    <>
      <section className="flex-col md:flex-row flex items-center md:justify-between mt-16 mb-16 md:mb-12">
        <h1 className="text-6xl md:text-8xl font-bold tracking-tighter leading-tight md:pr-8">
          Tools.
        </h1>
      </section>
      <section>
        <div className="grid grid-cols-2 row-gap-12">
          {tools.map((tool) => (
            <div key={tool.link}>
              <h3 className="text-3xl mb-3 leading-snug">
                <Link href={`/tools/${tool.link}`}>
                  <a className="hover:underline">{tool.name}</a>
                </Link>
              </h3>
              <p className="text-lg leading-relaxed mb-4">{tool.description}</p>
            </div>
          ))}
        </div>
      </section>
    </>
  );
}
