import React from 'react';

export default function AboutPageContent() {
  return (
    <>
      <section className="flex-col md:flex-row flex items-center md:justify-between mt-16 mb-16 md:mb-12">
        <h1 className="text-6xl md:text-8xl font-bold tracking-tighter leading-tight md:pr-8">
          About me and this blog.
        </h1>
      </section>
      <p>
        I have been investing since I was 16 years old when I opened my first brokerage account with Vanguard.
        I remember searching something on Google to the effect of "how to make money", which led to a website about investing and letting your money work for you.
        Back then I had barely any money to invest, but I understood what compound interest was and the power behind it.
        That initial investment still has not made me much money in the grand scheme of things, but it has made me some returns which is better than just depreciating.
        After I started going down that path, there was no going back.
        For me seeing my investments gain value was a sort of addiction.
        I personally think the reason the rich get richer and the poor get poorer is whether or not that person becomes addicted to making or spending money.
        My goal is to make you addicted to making money and become an investor.
      </p>
      <p>
        You can not make money if you spend it all.
        "Money comes and money goes" is an awful mindset.
        Money does not have a mind of its own.
        Once you possess that money, you choose where it goes.
        I believe that people do not have an earning problem but a spending problem.
        Adjusting your lifestyle to live below your means can help you put more money away, which means that you can have more money sitting and compounding.
        Frugality plays a huge part in starting your journey, and I hope to give some tips about saving money in addition to making it.
      </p>
      <p>
        I do not know everything and I never will.
        My goal is to impart as much knowledge as I have learned from books, other people, and personal experiences.
        I have strong opinions about how to handle your money that will undoubtedly come through.
        I let my opinions be known because I have formed them carefully and after much learning.
        While every person is in a unique situation, I believe it is important to still stick to a general rule of spending less than you make and investing the difference.
        My goal through my writing is to view financial situations the way I think they would best be handled and to add my voice to the gigantic world of personal finance.
        That being said I am not setting out to justify actions, and my opinions will most likely offend people who are in a different position in life than I am.
        Please do not take anything personal because this is all for entertainment and hopefully education.
      </p>
      <p>
        Some of my posts will be tied to the title.
        Some of my posts will start on a topic and veer off in a stream of consciousness.
        Some of my posts will turn into rants.
        It's a grab bag.
        My goal of writing all of this and making it public is to simply think and learn in public.
        Hopefully you can gain something useful from all of it.
      </p>
    </>
  );
}
