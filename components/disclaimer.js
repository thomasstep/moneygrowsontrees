import React from 'react';

export default function DisclaimerPageContent() {
  return (
    <>
      <section className="flex-col md:flex-row flex items-center md:justify-between mt-16 mb-16 md:mb-12">
        <h1 className="text-6xl md:text-8xl font-bold tracking-tighter leading-tight md:pr-8">
          Disclaimer.
        </h1>
      </section>
      <p>
        We try to provide accurate information on personal finance and investing, but it may not apply directly to your individual situation.
        We are not a financial advisor and we recommend you consult with a financial professional before making any serious financial decisions.
        The content on this site is for informational and educational purposes only and should not be construed as professional financial advice.
        Should you need such advice, consult a licensed financial or tax advisor.
        We do not collect any personal information about you as a visitor except for the anonymous data being collected by Google Analytics – which we use to monitor our website traffic.
      </p>
    </>
  );
}
